package com.example.a20190602_kwawannan_nycschools.Utils;

import android.app.ProgressDialog;
import android.content.Context;

public class Util {
   public static ProgressDialog pDialog;

    public static ProgressDialog showProgressDialog(Context context) {
         pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
        return pDialog;
    }

    public static void hideProgressDialog(ProgressDialog pDialog) {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
