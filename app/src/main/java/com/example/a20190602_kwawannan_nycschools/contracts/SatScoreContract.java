package com.example.a20190602_kwawannan_nycschools.contracts;

import android.app.ProgressDialog;

import com.example.a20190602_kwawannan_nycschools.model.SatScore;

public interface SatScoreContract {

    interface View {
        void initSatScoreActivityViews();

        void updateSuccess(SatScore score);

        void updateFailure(String message);

        void setSatScore(SatScore score);

        void showLoadingDialog();

        void dismissLoadingDialog(ProgressDialog progressDialog);


    }

    interface Presenter {
        void addView(SatScoreContract.View view);

        void initView();

        void LoadDetails(String schoolID);


    }
}
